import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-validate-code',
  templateUrl: './validate-code.component.html',
  styleUrls: ['./validate-code.component.scss']
})
export class ValidateCodeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.loadModalMessage();
  }

  loadModalMessage(){
    Swal.fire({
      html:
      `
      <div class="container-passwordChange">
          <h2>Revisa tu correo y activa</h2>
          <img src="../../../assets/img/login/email.png">
          <p>Hemos enviado un link a tu correo para que actives tu cuenta.</p>
      </div>
      `,
      showConfirmButton: true,
      showCancelButton: false,
      allowOutsideClick: false,
      width: '600px',
      customClass: {
        container: 'session',
        cancelButton: 'btn-primary Gotham-notbold',
        confirmButton: 'btn-anybg Gotham-notbold'
      },
      buttonsStyling: false,
      confirmButtonText: 'Ok',

    });

  }

  resendCodeMessage(){

    Swal.fire({
      html:
      `
      <div class="container-passwordChange">
          <h2>Código Reenviado</h2>
      </div>
      `,
      showConfirmButton: true,
      showCancelButton: false,
      allowOutsideClick: false,
      width: '600px',
      customClass: {
        container: 'session',
        cancelButton: 'btn-primary Gotham-notbold',
        confirmButton: 'btn-anybg Gotham-notbold'
      },
      buttonsStyling: false,
      confirmButtonText: 'Ok',

    });

  }

}
