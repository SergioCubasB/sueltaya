import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { MaterialModule } from '../shared/material/material.module';
import { ValidateCodeComponent } from './validate-code/validate-code.component';
import { UpdatePasswordComponent } from './update-password/update-password.component';

@NgModule({
  declarations: [
    LoginComponent,
    ResetPasswordComponent,
    ValidateCodeComponent,
    UpdatePasswordComponent
  ],
  exports: [
    LoginComponent,
    ResetPasswordComponent

  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class AuthModule { }
