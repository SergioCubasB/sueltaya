import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit {
  hide = true;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {

  }
  validateSendChangePassword(){
    Swal.fire({
      html:
      `
      <div class="container-passwordChange">
          <h2>Contraseña Actualizada</h2>
          <img src="../../../assets/img/login/candado.png">
      </div>
      `,
      showConfirmButton: true,
      showCancelButton: false,
      allowOutsideClick: false,
      width: '600px',
      customClass: {
        container: 'session',
        cancelButton: 'btn-primary Gotham-notbold',
        confirmButton: 'btn-anybg Gotham-notbold'
      },
      buttonsStyling: false,
      confirmButtonText: '¡Continuar!',

    }).then((result) => {
      if (result.isConfirmed) {
        this.router.navigate(['/login']);
      }

    });

  }

}
