import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  menu: boolean = true;

  constructor(
  ) { }

  ngOnInit(): void {
  }

  menuOpenClose(menu: boolean) {
    this.menu = !menu;
  }

}
